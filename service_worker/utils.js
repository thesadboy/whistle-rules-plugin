export default {
  setTitle(title) {
    chrome.action.setTitle({
      title: `${chrome.i18n.getMessage('titleBackground')}${title}`
    });
  },
  refreshCurrentPage() {
    chrome.tabs.reload();
  },
  setBadge(text) {
    chrome.action.setBadgeBackgroundColor({color: '#34e194'})
    chrome.action.setBadgeText({text: text ? text.toString() : ''})
  }
}
