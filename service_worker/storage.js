const STORAGE_KEY = 'whistle-api-url'

const getApiUrl = async () => {
  return (await get(STORAGE_KEY)) || 'http://127.0.0.1:8899'
}
const setApiUrl = async (value) => {
  await set(STORAGE_KEY, value)
}
const removeApiUrl = async () => {
  await remove(STORAGE_KEY)
}

const get = async (key) => {
  return (await chrome.storage.local.get(key))[key]
}
const set = async (key, value) => {
  try {
    await chrome.storage.local.set({[key]: value})
  } catch (e) {
    console.log(e)
  }
}
const remove = async (key) => {
  await chrome.storage.local.remove(key)
}
export default {
  getApiUrl, setApiUrl, removeApiUrl, get, set, remove
}
