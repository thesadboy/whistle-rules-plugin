const {sendMessage} = chrome.runtime
export default {
  async getInitInfo() {
    return await sendMessage({scope: 'service', action: 'getInitInfo'})
  }, async setRuleEnable(item, isEnable) {
    return await sendMessage({scope: 'service', action: 'setRuleEnable', params: [item, isEnable]})
  }, async changeDefaultRule(isEnable) {
    return await sendMessage({scope: 'service', action: 'changeDefaultRule', params: [isEnable]})
  }, async setAllowMultipleChoice(isAllow) {
    return await sendMessage({scope: 'service', action: 'setAllowMultipleChoice', params: [isAllow]})
  }
}
