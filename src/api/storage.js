const {sendMessage} = chrome.runtime
export default {
  async getApiUrl() {
    return await sendMessage({scope: 'storage', action: 'getApiUrl'})
  },
  async setApiUrl(value) {
    return await sendMessage({scope: 'storage', action: 'setApiUrl', params: [value]})
  },
  async removeApiUrl() {
    return await sendMessage({scope: 'storage', action: 'removeApiUrl'})
  },
  async set(key, value) {
    return await sendMessage({scope: 'storage', action: 'set', params: [key, value]})
  },
  async get(key) {
    return await sendMessage({scope: 'storage', action: 'get', params: [key]})
  },
  async remove(key) {
    return await sendMessage({scope: 'storage', action: 'remove', params: [key]})

  }
}
