const {sendMessage} = chrome.runtime
export default {
  async enable() {
    return await sendMessage({scope: 'proxy', action: 'enable'})
  }, async disable() {
    return await sendMessage({scope: 'proxy', action: 'disable'})
  }, async get() {
    return await sendMessage({scope: 'proxy', action: 'get'})
  }, async initList() {
    return await sendMessage({scope: 'proxy', action: 'initList'})
  }
}
