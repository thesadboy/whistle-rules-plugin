export default {
  async setMenu() {
    return await chrome.runtime.sendMessage({scope: 'contextMenu', action: 'setMenu'})
  }
}
