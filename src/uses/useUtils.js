import {autoRefresh, proxyEnabled} from "./useInit.js";

const reload = async () => {
  if (autoRefresh.value && proxyEnabled.value) {
    await chrome.tabs.reload();
  }
};

export {
  reload
}
