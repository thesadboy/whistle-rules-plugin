import {defineConfig} from "vite";

const path = require('path');

export default defineConfig({
    build: {
        lib: {
            entry: 'service_worker/index.js',
            formats: ['es'],
            name: 'service_worker',
            fileName: () => 'service_worker.js'
        },
        emptyOutDir: false
    }
})
